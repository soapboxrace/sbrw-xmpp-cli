package io.github.sbcloudrace.sbxmppcli.lobby;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LobbySession implements Serializable {

    private Long lobbyId;

    private Integer eventId;

    private List<LobbySessionEntrant> entrants;

    private long timeToLive;

}
