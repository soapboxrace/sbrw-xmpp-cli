package io.github.sbcloudrace.sbxmppcli.lobby;


import io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp.*;
import io.github.sbcloudrace.sbxmppcli.service.SbXmppClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/lobby")
@RequiredArgsConstructor
public class LobbyController {

    private final SbXmppClient sbXmppClient;
    private final XmppLobbyService xmppLobbyService;

    @PutMapping(value = "/send-join-event/{lobbyInviteId}/{eventId}/{personaId}")
    @ResponseBody
    public void sendJoinEvent(@PathVariable Long lobbyInviteId,
                              @PathVariable Integer eventId,
                              @PathVariable Long personaId) {
        LobbyInvite lobbyInvite = new LobbyInvite();
        lobbyInvite.setEventId(eventId);
        lobbyInvite.setLobbyInviteId(lobbyInviteId);
        lobbyInvite.setInviteLifetimeInMilliseconds(10000);
        XMPP_ResponseType xmpp_responseType = new XMPP_ResponseType();
        xmpp_responseType.setLobbyInvite(lobbyInvite);
        sbXmppClient.send(xmpp_responseType, personaId);
    }

    @PutMapping("/start-lobby-event")
    @ResponseBody
    public void startLobbyEvent(@RequestBody LobbySession lobbySession,
                                @RequestParam String udpRaceIp,
                                @RequestParam Integer udpRacePort) {
        System.out.println("lobbyId: " + lobbySession.getLobbyId());
        System.out.println("eventId: " + lobbySession.getEventId());
        System.out.println("timeToLive: " + lobbySession.getTimeToLive());
        List<LobbySessionEntrant> entrants = lobbySession.getEntrants();
        if (entrants == null) {
            return;
        }
        entrants.forEach(entrant -> {
            System.out.println("personaId: " + entrant.getPersonaId());
            System.out.println("personaLevel: " + entrant.getPersonaLevel());
        });

        if (entrants.size() < 2) {
            return;
        }
        XMPP_LobbyLaunchedType lobbyLaunched = new XMPP_LobbyLaunchedType();
        Entrants entrantsType = new Entrants();
        List<LobbyEntrantInfo> lobbyEntrantInfo = entrantsType.getLobbyEntrantInfo();
        XMPP_CryptoTicketsType xMPP_CryptoTicketsType = new XMPP_CryptoTicketsType();
        List<XMPP_P2PCryptoTicketType> p2pCryptoTicket = xMPP_CryptoTicketsType.getP2PCryptoTicket();
        int i = 0;

        for (LobbySessionEntrant lobbyEntrantEntity : entrants) {
            Long personaId = lobbyEntrantEntity.getPersonaId();

            XMPP_P2PCryptoTicketType p2pCryptoTicketType = new XMPP_P2PCryptoTicketType();
            p2pCryptoTicketType.setPersonaId(personaId);
            p2pCryptoTicketType.setSessionKey("AAAAAAAAAAAAAAAAAAAAAA==");
            p2pCryptoTicket.add(p2pCryptoTicketType);

            LobbyEntrantInfo lobbyEntrantInfoType = new LobbyEntrantInfo();
            lobbyEntrantInfoType.setPersonaId(personaId);
            lobbyEntrantInfoType.setLevel(lobbyEntrantEntity.getPersonaLevel());
            lobbyEntrantInfoType.setHeat(1);
            lobbyEntrantInfoType.setGridIndex(i++);
            lobbyEntrantInfoType.setState(LobbyEntrantState.UNKNOWN);

            lobbyEntrantInfo.add(lobbyEntrantInfoType);
        }
        XMPP_EventSessionType xMPP_EventSessionType = new XMPP_EventSessionType();
        ChallengeType challengeType = new ChallengeType();
        challengeType.setChallengeId("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        challengeType.setPattern("FFFFFFFFFFFFFFFF");
        challengeType.setLeftSize(14);
        challengeType.setRightSize(50);

        xMPP_EventSessionType.setEventId(lobbySession.getEventId());
        xMPP_EventSessionType.setChallenge(challengeType);
        xMPP_EventSessionType.setSessionId(lobbySession.getLobbyId());
        lobbyLaunched.setNewRelayServer(true);
        lobbyLaunched.setLobbyId(lobbySession.getLobbyId());
        lobbyLaunched.setUdpRelayHost(udpRaceIp);
        lobbyLaunched.setUdpRelayPort(udpRacePort);

        lobbyLaunched.setEntrants(entrantsType);

        lobbyLaunched.setEventSession(xMPP_EventSessionType);

        xmppLobbyService.sendRelay(lobbyLaunched, xMPP_CryptoTicketsType);
    }

    @PutMapping("/send-join-msg")
    @ResponseBody
    public void sendJoinMsg(@RequestBody LobbySessionEntrant lobbySessionEntrant,
                            @RequestParam Long lobbyId,
                            @RequestParam Long personaId) {
        LobbyEntrantAdded lobbyEntrantAdded = new LobbyEntrantAdded();
        lobbyEntrantAdded.setLobbyId(lobbyId);
        lobbyEntrantAdded.setPersonaId(lobbySessionEntrant.getPersonaId());
        lobbyEntrantAdded.setLevel(lobbySessionEntrant.getPersonaLevel());
        lobbyEntrantAdded.setHeat(1);
        lobbyEntrantAdded.setGridIndex(1);
        lobbyEntrantAdded.setState(LobbyEntrantState.UNKNOWN);
        xmppLobbyService.sendJoinMsg(lobbyEntrantAdded, personaId);
    }
}
