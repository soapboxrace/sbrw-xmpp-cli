package io.github.sbcloudrace.sbxmppcli.lobby;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LobbySessionEntrant {

    private Long personaId;

    private Integer personaLevel;
    
}
