package io.github.sbcloudrace.sbxmppcli.service;

import io.github.sbcloudrace.sbxmppcli.cli.SubjectCalc;
import io.github.sbcloudrace.sbxmppcli.cli.XmppTalk;
import io.github.sbcloudrace.sbxmppcli.cli.jaxb.util.MarshalXML;
import io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp.XMPP_MessageType;
import io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp.XMPP_ResponseType;
import io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp.XMPP_ResponseTypeEntrantAdded;
import io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp.XMPP_ResponseTypeLobbyLaunched;
import io.github.sbcloudrace.sbxmppcli.compoment.Handshake;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class SbXmppClient {

    private final Handshake handshake;
    private final XmppTalk xmppTalk;

    public SbXmppClient(Handshake handshake) {
        this.handshake = handshake;
        this.xmppTalk = handshake.getXmppTalk();
    }

    private void send(String msg, Long to) {
        XMPP_MessageType messageType = new XMPP_MessageType();
        messageType.setToPersonaId(to, handshake.getXmppConfig().getFdqn());
        messageType.setBody(msg);
        messageType.setSubject(SubjectCalc.calculateHash(messageType.getTo().toCharArray(), msg.toCharArray()));
        String packet = MarshalXML.marshal(messageType);
        xmppTalk.write(packet);
    }

    public void send(XMPP_ResponseType xmppResponseType, Long to) {
        String responseXmlStr = MarshalXML.marshal(xmppResponseType);
        this.send(responseXmlStr, to);
    }

    public void send(XMPP_ResponseTypeLobbyLaunched xmppResponseType, Long to) {
        String responseXmlStr = MarshalXML.marshal(xmppResponseType);
        this.send(responseXmlStr, to);
    }

    public void send(XMPP_ResponseTypeEntrantAdded xmppResponseType, Long to) {
        String responseXmlStr = MarshalXML.marshal(xmppResponseType);
        this.send(responseXmlStr, to);
    }

}
