package io.github.sbcloudrace.sbxmppcli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SbXmppCliApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbXmppCliApplication.class, args);
    }

}
