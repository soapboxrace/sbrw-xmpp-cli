package io.github.sbcloudrace.sbxmppcli.openfire;

import feign.RequestInterceptor;
import io.github.sbcloudrace.sbxmppcli.config.XmppConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;

@RequiredArgsConstructor
public class OpenfireFeignConfig {

    private final XmppConfig xmppConfig;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header("Accept", "application/json");
            requestTemplate.header("Authorization", xmppConfig.getToken());
        };
    }
}
