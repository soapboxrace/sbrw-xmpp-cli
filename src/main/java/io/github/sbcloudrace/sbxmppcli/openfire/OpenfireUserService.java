package io.github.sbcloudrace.sbxmppcli.openfire;

import lombok.RequiredArgsConstructor;
import org.igniterealtime.restclient.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OpenfireUserService {

    private final OpenfireUserServiceProxy openfireUserServiceProxy;

    public void createAllPersonasXmpp(List<Long> personaIds,
                                      String password) {
        personaIds.forEach(aLong -> {
            String xmppUserName = "sbrw.".concat(aLong.toString());
            try {
                UserEntity userEntityTmp = openfireUserServiceProxy.get(xmppUserName);
                userEntityTmp.setPassword(password);
                openfireUserServiceProxy.update(userEntityTmp, xmppUserName);
            } catch (Exception e) {
                UserEntity userEntity = new UserEntity(
                        xmppUserName,
                        null,
                        null,
                        password);
                openfireUserServiceProxy.create(userEntity);
            }
        });
    }
}
