package io.github.sbcloudrace.sbxmppcli.openfire;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class OpenfireUserController {

    private final OpenfireUserService openfireUserService;

    @PutMapping(value = "/{password}")
    @ResponseBody
    public void createAllPersonasXmpp(@RequestBody List<Long> personaIds,
                                      @PathVariable String password) {
        openfireUserService.createAllPersonasXmpp(personaIds, password);
    }
}
