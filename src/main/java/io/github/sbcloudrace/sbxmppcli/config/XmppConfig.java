package io.github.sbcloudrace.sbxmppcli.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("xmpp")
@Getter
@Setter
public class XmppConfig {

    public String fdqn;
    public Integer port;
    public String token;
    public String url;

}
