package io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp;

import jakarta.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CryptoTicketsType", propOrder = { "p2PCryptoTicket" })
public class XMPP_CryptoTicketsType {

	@XmlElement(name = "P2PCryptoTicket")
	protected List<XMPP_P2PCryptoTicketType> p2PCryptoTicket;

	public List<XMPP_P2PCryptoTicketType> getP2PCryptoTicket() {
		if (p2PCryptoTicket == null) {
			p2PCryptoTicket = new ArrayList<XMPP_P2PCryptoTicketType>();
		}
		return this.p2PCryptoTicket;
	}

}
