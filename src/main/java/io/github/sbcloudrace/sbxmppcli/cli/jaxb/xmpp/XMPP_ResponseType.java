package io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp;

import jakarta.xml.bind.annotation.*;
import lombok.Getter;
import lombok.Setter;


@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
@XmlRootElement(name = "response")
public class XMPP_ResponseType {

    @XmlElement(name = "PowerupActivated")
    private PowerupActivated powerupActivated;

    @XmlElement(name = "LobbyInvite")
    private LobbyInvite lobbyInvite;

    @XmlAttribute(name = "status")
    private int status = 1;
    @XmlAttribute(name = "ticket")
    private int ticket = 0;

}
