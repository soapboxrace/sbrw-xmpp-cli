package io.github.sbcloudrace.sbxmppcli.cli.jaxb.xmpp;

import lombok.Getter;
import lombok.Setter;

import jakarta.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XMPP_IQPongType", propOrder = {"from", "to", "id", "type"})
@XmlRootElement(name = "iq")
@Getter
@Setter
public class XMPP_IQPongType {
    @XmlAttribute(name = "from")
    private String from;
    @XmlAttribute(name = "to")
    private String to;
    @XmlAttribute(name = "id")
    private String id;
    @XmlAttribute(name = "type", required = true)
    private String type = "result";

    public XMPP_IQPongType() {
    }

    public XMPP_IQPongType(String id, String toFqdn) {
        from = String.format("sbrw.engine.engine@%s/EA_Chat", toFqdn);
        to = toFqdn;
        this.id = id;
    }


}
