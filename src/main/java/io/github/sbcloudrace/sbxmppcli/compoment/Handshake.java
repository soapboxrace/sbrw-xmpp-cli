package io.github.sbcloudrace.sbxmppcli.compoment;

import io.github.sbcloudrace.sbxmppcli.cli.SocketClient;
import io.github.sbcloudrace.sbxmppcli.cli.TlsWrapper;
import io.github.sbcloudrace.sbxmppcli.cli.XmppTalk;
import io.github.sbcloudrace.sbxmppcli.config.XmppConfig;
import io.github.sbcloudrace.sbxmppcli.openfire.OpenfireUserServiceProxy;
import lombok.Getter;
import org.igniterealtime.restclient.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
@Getter
public class Handshake {

    private final XmppConfig xmppConfig;

    private final XmppTalk xmppTalk;

    public Handshake(OpenfireUserServiceProxy openfireUserServiceProxy, XmppConfig xmppConfig) {
        String xmppUserName = "sbrw.engine.engine";
        try {
            UserEntity userEntity = openfireUserServiceProxy.get(xmppUserName);
            userEntity.setPassword(xmppConfig.getToken());
            openfireUserServiceProxy.update(userEntity, xmppUserName);
        } catch (Exception e) {
            UserEntity userEntity = new UserEntity(
                    xmppUserName,
                    null,
                    null,
                    xmppConfig.getToken());
            openfireUserServiceProxy.create(userEntity);
        }

        this.xmppConfig = xmppConfig;
        String xmppIp = xmppConfig.getFdqn();
        int xmppPort = xmppConfig.getPort();

        SocketClient socketClient = new SocketClient(xmppIp, xmppPort);
        socketClient.send(
                "<?xml version='1.0' ?><stream:stream to='" + xmppIp + "' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0' xml:lang='en'>");
        String receive = socketClient.receive();
        while (!receive.contains("</stream:features>")) {
            receive = socketClient.receive();
        }
        socketClient.send("<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>");
        socketClient.receive();
        xmppTalk = new XmppTalk(socketClient.getSocket(), xmppIp);
        TlsWrapper.wrapXmppTalk(xmppTalk);
        xmppTalk.write(
                "<?xml version='1.0' ?><stream:stream to='" + xmppIp + "' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version='1.0' xml:lang='en'>");
        xmppTalk.write("<iq id='EA-Chat-1' type='get'><query xmlns='jabber:iq:auth'><username>sbrw.engine.engine</username></query></iq>");
        xmppTalk.read();
        xmppTalk.write("<iq xml:lang='en' id='EA-Chat-2' type='set'><query xmlns='jabber:iq:auth'><username>sbrw.engine.engine</username><password>" + xmppConfig.getToken()
                + "</password><resource>EA_Chat</resource><clientlock xmlns='http://www.jabber.com/schemas/clientlocking.xsd' id='900'>57b8914527daff651df93557aef0387e5aa60fae</clientlock></query></iq>");
        xmppTalk.read();
        xmppTalk.write("<presence><show>chat</show><status>Online</status><priority>0</priority></presence>");
        xmppTalk.write(" ");
        xmppTalk.write("<presence to='channel.CMD__1@conference." + xmppIp + "/sbrw.engine.engine'/>");
        xmppTalk.startReader();
    }

}