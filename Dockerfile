FROM eclipse-temurin:21.0.3_9-jdk-alpine as build

COPY ./ /build
WORKDIR /build

RUN ./mvnw clean package

# stage 2
FROM eclipse-temurin:21.0.3_9-jre-alpine

COPY --from=build /build/target/sbrw-xmpp-cli.jar /app/application.jar
WORKDIR /app
RUN adduser -S appuser
USER appuser
EXPOSE 8201
ENV JAVA_OPTS="-Xms256m -Xmx512m"
ENV XMPP_URL="http://localhost:9090"
ENV XMPP_TOKEN="superSecret2"
ENV XMPP_PORT=5222
ENV XMPP_FDQN=localhost

CMD java ${JAVA_OPTS} -jar application.jar
